package br.senai.sp.informatica.usandofragments;

import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Boolean status = true;
    private Button botaoTrocar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botaoTrocar = findViewById(R.id.btnTrocarFragments);
        botaoTrocar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                if (status) {
                    LoginFragment loginFragment = new LoginFragment();
                    fragmentTransaction.replace(R.id.container_fragments, loginFragment);
                    botaoTrocar.setText("Cadastro");
                    fragmentTransaction.commit();
                    status = false;
                }else{
                    CadastroFragment cadastroFragment = new CadastroFragment();
                    fragmentTransaction.replace(R.id.container_fragments, cadastroFragment);
                    botaoTrocar.setText("Login");
                    fragmentTransaction.commit();
                    status = true;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }
}
