package br.com.senai.pessoafisicaoujuridica;

/**
 * Created by 17682202807 on 23/02/2018.
 */

class Juridica {

    private Long id;
    private String cnpj;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @Override
    public String toString(){
        return getId() + "\n" + "Cpf: " + getCnpj();
    }
}
