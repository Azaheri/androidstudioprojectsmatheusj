package br.com.senai.pessoafisicaoujuridica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PessoaFisicaActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView Nome;
    private EditText cpf;
    private Button Cadastrar;
    private FisicaHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pessoa_fisica);

        Nome = findViewById(R.id.txtNome);
        cpf = findViewById(R.id.edtCpf);
        Cadastrar = findViewById(R.id.btnPeFisica);
        helper = new FisicaHelper(this);
        Cadastrar.setOnClickListener(this);

        Bundle extra = getIntent().getExtras();

        if (extra != null){
            String textoNome = extra.getString("nomezinho");
            Nome.setText(textoNome);
        }


    }

    @Override
    public void onClick(View v) {
        FisicaDAO dao = new FisicaDAO(PessoaFisicaActivity.this);
        Fisica fisica = helper.pegaFisica();
        dao.insere(fisica);
        dao.close();
        Intent intent = new Intent(PessoaFisicaActivity.this, ListaFisicaActivity.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Pessoa fisica: " + fisica.getCpf() + " salva!", Toast.LENGTH_LONG).show();
        finish();
    }
}
