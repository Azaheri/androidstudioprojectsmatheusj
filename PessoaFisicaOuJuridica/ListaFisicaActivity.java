package br.com.senai.pessoafisicaoujuridica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListaFisicaActivity extends AppCompatActivity{

    private ListView listaFisica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_fisica);

        listaFisica = findViewById(R.id.lvFisica);
        CarregarLista();
    }

    private void CarregarLista() {
        FisicaDAO dao = new FisicaDAO(this);
        List<Fisica> fisicas = dao.buscarFisica();
        ArrayAdapter<Fisica> adaptador = new ArrayAdapter<Fisica>(this, android.R.layout.simple_list_item_1, fisicas);
        listaFisica.setAdapter(adaptador);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CarregarLista();
    }

}
