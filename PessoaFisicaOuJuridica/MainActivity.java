package br.com.senai.pessoafisicaoujuridica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button botaoCadastro;
    private Button botaoFisica;
    private Button botaoJuridica;
    private EditText Nome;
    private CheckBox Fisica;
    private CheckBox Juridica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoCadastro = findViewById(R.id.btnCadastrar);
        botaoFisica = findViewById(R.id.btnFisica);
        botaoJuridica = findViewById(R.id.btnJuridica);
        Nome = findViewById(R.id.edtNome);
        Fisica = findViewById(R.id.cbFisica);
        Juridica = findViewById(R.id.cbJuridica);

        botaoCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nomezinho = Nome.getText().toString();
                if (Fisica.isChecked() && Juridica.isChecked()){
                    Toast.makeText(getApplicationContext(), "Selecione apenas uma das opções", Toast.LENGTH_LONG).show();
                }else if (Fisica.isChecked()){
                    Intent intent = new Intent(MainActivity.this, PessoaFisicaActivity.class);
                    intent.putExtra("nomezinho", nomezinho);
                    startActivity(intent);
                }else if (Juridica.isChecked()){
                    Intent intent = new Intent(MainActivity.this, PessoaJuridicaActivity.class);
                    intent.putExtra("nomezinho", nomezinho);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "Selecione uma das opções", Toast.LENGTH_LONG).show();
                }
            }
        });

        botaoFisica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaFisicaActivity.class);
                startActivity(intent);
            }
        });

        botaoJuridica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaJuridicaActivity.class);
                startActivity(intent);
            }
        });
    }
}
