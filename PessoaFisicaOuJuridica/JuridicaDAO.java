package br.com.senai.pessoafisicaoujuridica;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 17682202807 on 23/02/2018.
 */

public class JuridicaDAO extends SQLiteOpenHelper {


    public JuridicaDAO(Context context) {
        super(context, "ListaJuridica", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Juridica(id INTEGER PRIMARY KEY, cnpj TEXT NOT NULL)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Juridica";
        db.execSQL(sql);
    }

    public void insere(Juridica juridica) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = new ContentValues();

        dados.put("nome", juridica.getCnpj());
        db.insert("Juridica",null, dados);
    }

    public List<Juridica> buscarJuridica() {
        String sql = "SELECT * FROM Juridica";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql,null);
        List<Juridica> juridicas = new ArrayList<Juridica>();
        while (c.moveToNext()){
            Juridica juridica = new Juridica();
            juridica.setId(c.getLong(c.getColumnIndex("id")));
            juridica.setCnpj(c.getString(c.getColumnIndex("cnpj")));
            juridicas.add(juridica);
        }
        return juridicas;
    }
}
