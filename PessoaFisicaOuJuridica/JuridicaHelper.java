package br.com.senai.pessoafisicaoujuridica;

import android.widget.EditText;

/**
 * Created by 17682202807 on 23/02/2018.
 */

public class JuridicaHelper {

    private EditText cnpj;

    public JuridicaHelper(PessoaJuridicaActivity formularioJuridica){

        cnpj = formularioJuridica.findViewById(R.id.edtCnpj);
    }


    public Juridica pegaJuridica() {

        Juridica juridica = new Juridica();
        juridica.setCnpj(cnpj.getText().toString());
        return juridica;
    }
}
