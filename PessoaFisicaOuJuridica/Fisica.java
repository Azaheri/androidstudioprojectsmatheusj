package br.com.senai.pessoafisicaoujuridica;

/**
 * Created by 17682202807 on 23/02/2018.
 */

class Fisica {

    private Long id;
    private String cpf;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString(){
        return getId() + "\n" + "Cpf: " + getCpf();
    }
}
