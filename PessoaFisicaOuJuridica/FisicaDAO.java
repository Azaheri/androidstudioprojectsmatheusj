package br.com.senai.pessoafisicaoujuridica;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 17682202807 on 23/02/2018.
 */

public class FisicaDAO extends SQLiteOpenHelper{


    public FisicaDAO(Context context) {
        super(context, "ListaFisica", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Fisica(id INTEGER PRIMARY KEY, cpf TEXT NOT NULL)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Fisica";
        db.execSQL(sql);
    }

    public void insere(Fisica fisica) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = new ContentValues();

        dados.put("cpf", fisica.getCpf());
        db.insert("Fisica",null, dados);
    }

    public List<Fisica> buscarFisica() {
        String sql = "SELECT * FROM Fisica";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql,null);
        List<Fisica> fisicas = new ArrayList<Fisica>();
        while (c.moveToNext()){
            Fisica fisica = new Fisica();
            fisica.setId(c.getLong(c.getColumnIndex("id")));
            fisica.setCpf(c.getString(c.getColumnIndex("cpf")));
            fisicas.add(fisica);
        }
        return fisicas;
    }
}
