package br.com.senai.pessoafisicaoujuridica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListaJuridicaActivity extends AppCompatActivity {

    private ListView listaJuridica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_juridica);

        listaJuridica = findViewById(R.id.lvJuridica);
        CarregarLista();
    }

    private void CarregarLista() {
        JuridicaDAO dao = new JuridicaDAO(this);
        List<Juridica> juridicas = dao.buscarJuridica();
        ArrayAdapter<Juridica> adaptador = new ArrayAdapter<Juridica>
                (this, android.R.layout.simple_list_item_1, juridicas);
        listaJuridica.setAdapter(adaptador);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CarregarLista();
    }
}
