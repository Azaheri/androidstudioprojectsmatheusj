package br.com.senai.pessoafisicaoujuridica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PessoaJuridicaActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView Nome;
    private EditText cnpj;
    private Button Cadastro;
    private JuridicaHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pessoa_juridica);

        Nome = findViewById(R.id.txtNome);
        cnpj = findViewById(R.id.edtCnpj);
        Cadastro = findViewById(R.id.btnPeJuridica);
        helper = new JuridicaHelper(this);
        Cadastro.setOnClickListener(this);

        Bundle extra = getIntent().getExtras();

        if (extra != null){
            String textoNome = extra.getString("nomezinho");
            Nome.setText(textoNome);
        }
    }

    @Override
    public void onClick(View v) {
        JuridicaDAO dao = new JuridicaDAO(PessoaJuridicaActivity.this);
        Juridica juridica = helper.pegaJuridica();
        dao.insere(juridica);
        dao.close();
        Intent intent = new Intent(PessoaJuridicaActivity.this, ListaJuridicaActivity.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Pessoa juridica: " + juridica.getCnpj() + " salva!", Toast.LENGTH_LONG).show();
        finish();
    }
}
