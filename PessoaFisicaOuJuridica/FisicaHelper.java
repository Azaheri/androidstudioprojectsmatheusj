package br.com.senai.pessoafisicaoujuridica;

import android.widget.EditText;

/**
 * Created by 17682202807 on 23/02/2018.
 */

public class FisicaHelper {

    private EditText cpf;

    public FisicaHelper(PessoaFisicaActivity formularioFisica){

        cpf = formularioFisica.findViewById(R.id.edtCpf);
    }


    public Fisica pegaFisica() {

        Fisica fisica = new Fisica();
        fisica.setCpf(cpf.getText().toString());
        return fisica;
    }
}
