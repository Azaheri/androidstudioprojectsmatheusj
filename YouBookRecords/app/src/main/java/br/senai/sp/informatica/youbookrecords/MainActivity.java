package br.senai.sp.informatica.youbookrecords;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import br.senai.sp.informatica.youbookrecords.DAO.BookDAO;
import br.senai.sp.informatica.youbookrecords.adapter.BookAdapter;
import br.senai.sp.informatica.youbookrecords.model.Book;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener{

    private RecyclerView bookList;
    private SearchView searchField;
    private BookAdapter bookAdapter;
    private BookDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bookList = findViewById(R.id.recyclerListId);
        searchField = findViewById(R.id.searchFieldId);
        searchField.setQueryHint("Type a book name!");
        searchField.setOnQueryTextListener(MainActivity.this);
        dao = new BookDAO(this);
        loadList(dao.searchBooks());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BookFormActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadList(List<Book> books) {
        bookAdapter = new BookAdapter(this, books);
        bookList.setAdapter(bookAdapter);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);
        bookList.setLayoutManager(layoutManager);
        bookList.setNestedScrollingEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        BookDAO dao = new BookDAO(this);
        loadList(dao.searchByName(newText));
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        BookDAO dao = new BookDAO(this);
        loadList(dao.searchBooks());
    }
}
