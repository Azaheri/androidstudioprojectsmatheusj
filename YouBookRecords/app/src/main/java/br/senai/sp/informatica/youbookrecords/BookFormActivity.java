package br.senai.sp.informatica.youbookrecords;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import br.senai.sp.informatica.youbookrecords.DAO.BookDAO;
import br.senai.sp.informatica.youbookrecords.helper.BookFormHelper;
import br.senai.sp.informatica.youbookrecords.model.Book;

public class BookFormActivity extends AppCompatActivity {

    private Button addBook;
    private BookFormHelper helper;
    private ImageView loadedImage;
    private BookDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_form);

        addBook = findViewById(R.id.btnRegister);
        helper = new BookFormHelper(this);
        dao = new BookDAO(getApplicationContext());
        loadedImage = findViewById(R.id.editImage);

        loadedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

        final Bundle extras = getIntent().getExtras();
        Long bookId = (extras != null) ? extras.getLong("bookId") : null;
        if (bookId == null){
            Book book = new Book();
        }else{
            Book bookFound = dao.find(bookId);
            helper.fillForm(bookFound);
        }
        addBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookDAO dao = new BookDAO(BookFormActivity.this);
                Book book = helper.getBook();
                book.setImagePath(loadedImage.getTag().toString());

                if(book.getId() != null){
                    dao.alter(book);
                }else{
                    dao.insert(book);
                }
                dao.close();
                Toast.makeText(BookFormActivity.this, "Book: " + book.getName() + " was saved!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1); // Control Alt C -> converte alguma coisa em constante
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1){
            Uri uri = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = getContentResolver().query(uri, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String imagePath = c.getString(columnIndex);
            c.close();
            Bitmap imgReturn = (BitmapFactory.decodeFile(imagePath));
            loadedImage.setImageBitmap(imgReturn);
            loadedImage.setTag(imagePath);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // / A permissão foi concedida. Pode continuar
            } else {
                // A permissão foi negada. Precisa ver o que deve ser desabilitado

            }
            return;
        }
    }
}
