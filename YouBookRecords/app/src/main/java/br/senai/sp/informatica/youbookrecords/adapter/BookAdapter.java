package br.senai.sp.informatica.youbookrecords.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.senai.sp.informatica.youbookrecords.R;
import br.senai.sp.informatica.youbookrecords.holder.BookViewHolder;
import br.senai.sp.informatica.youbookrecords.model.Book;

public class BookAdapter extends RecyclerView.Adapter{

    private final Context context;
    private final List<Book> books;

    public BookAdapter(Context context, List<Book> books) {
        this.context = context;
        this.books = books;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list, parent, false);
        BookViewHolder holder = new BookViewHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder myHolder = (BookViewHolder) holder;
        Book book = books.get(position);
        myHolder.fill(book);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }
}
