package br.senai.sp.informatica.youbookrecords.helper;

import android.widget.EditText;
import android.widget.ImageView;

import br.senai.sp.informatica.youbookrecords.BookFormActivity;
import br.senai.sp.informatica.youbookrecords.R;
import br.senai.sp.informatica.youbookrecords.model.Book;

public class BookFormHelper {

    private ImageView pic;
    private EditText name;
    private EditText author;
    private EditText genre;
    private EditText publisher;
    private EditText publishedDate;
    private Book book;

    public BookFormHelper(BookFormActivity bfa){
        pic = bfa.findViewById(R.id.editImage);
        name = bfa.findViewById(R.id.editName);
        author = bfa.findViewById(R.id.editAuthor);
        genre = bfa.findViewById(R.id.editGenre);
        publisher = bfa.findViewById(R.id.editPublisher);
        publishedDate = bfa.findViewById(R.id.editPublishedDate);
        book = new Book();
    }

    public Book getBook(){
        book.setName(name.getText().toString());
        book.setAuthor(author.getText().toString());
        book.setGenre(genre.getText().toString());
        book.setPublisher(publisher.getText().toString());
        book.setPublishedDate(publishedDate.getText().toString());
        return book;
    }

    public void fillForm(Book book){
        book.setName(book.getName());
        book.setAuthor(book.getAuthor());
        book.setGenre(book.getGenre());
        book.setPublisher(book.getPublisher());
        book.setPublishedDate(book.getPublishedDate());
        this.book = book;
    }
}
