package br.senai.sp.informatica.youbookrecords.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.youbookrecords.model.Book;

public class BookDAO extends SQLiteOpenHelper{
    public BookDAO(Context context) {
        super(context, "YBR", null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Book(id INTEGER PRIMARY KEY, name TEXT NOT NULL, author TEXT NOT NULL, genre TEXT NOT NULL, publisher TEXT NOT NULL, publishedDate TEXT NOT NULL, imagePath TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Book";
        db.execSQL(sql);
    }

    public void insert(Book book){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues data = getBookData(book);
        db.insert("Book", null, data);
    }

    @NonNull
    private ContentValues getBookData(Book book){
        ContentValues data = new ContentValues();
        data.put("name", book.getName());
        data.put("author", book.getAuthor());
        data.put("genre", book.getGenre());
        data.put("publisher", book.getPublisher());
        data.put("publishedDate", book.getPublishedDate());
        data.put("imagePath", book.getImagePath());
        return data;
    }

    public List<Book> searchBooks(){
        String sql = "SELECT * FROM Book";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Book> books = new ArrayList<>();

        while(c.moveToNext()){
            Book book = new Book();
            book.setId(c.getLong(c.getColumnIndex("id")));
            book.setName(c.getString(c.getColumnIndex("name")));
            book.setAuthor(c.getString(c.getColumnIndex("author")));
            book.setGenre(c.getString(c.getColumnIndex("genre")));
            book.setPublisher(c.getString(c.getColumnIndex("publisher")));
            book.setPublishedDate(c.getString(c.getColumnIndex("publishedDate")));
            book.setImagePath(c.getString(c.getColumnIndex("imagePath")));

            books.add(book);
        }
        return books;
    }

    public Book find(Long bookId){
        String sql = "SELECT * FROM Book WHERE id = ?";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(bookId)});
        c.moveToFirst();
        Book foundBook = new Book();
        foundBook.setId(c.getLong(c.getColumnIndex("id")));
        foundBook.setName(c.getString(c.getColumnIndex("name")));
        foundBook.setAuthor(c.getString(c.getColumnIndex("author")));
        foundBook.setGenre(c.getString(c.getColumnIndex("genre")));
        foundBook.setPublisher(c.getString(c.getColumnIndex("publisher")));
        foundBook.setPublishedDate(c.getString(c.getColumnIndex("publishedDate")));
        foundBook.setImagePath(c.getString(c.getColumnIndex("imagePath")));
        db.close();
        return foundBook;
    }

    public void delete(Book book){
        SQLiteDatabase db = getWritableDatabase();
        String[] param = {book.getId().toString()};
        db.delete("Book", "id = ?", param);
    }

    public void alter(Book book){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues data = getBookData(book);
        String[] param = {book.getId().toString()};
        db.update("Book", data, "id = ?", param);
    }

    public List<Book> searchByName(String searchedName){
        SQLiteDatabase db = getWritableDatabase();
        try{
            String sql = "SELECT * FROM Book WHERE name LIKE'"+searchedName+"%'";
            Cursor c = db.rawQuery(sql, null);
            List<Book> bookSearch = new ArrayList<>();

            while(c.moveToNext()){
                Book book = new Book();
                book.setId(c.getLong(c.getColumnIndex("id")));
                book.setName(c.getString(c.getColumnIndex("name")));
                book.setAuthor(c.getString(c.getColumnIndex("author")));
                book.setGenre(c.getString(c.getColumnIndex("genre")));
                book.setPublisher(c.getString(c.getColumnIndex("publisher")));
                book.setPublishedDate(c.getString(c.getColumnIndex("publishedDate")));
                book.setImagePath(c.getString(c.getColumnIndex("imagePath")));

                bookSearch.add(book);
            }
            c.close();
            return bookSearch;
        }finally {
            db.close();
        }
    }
}