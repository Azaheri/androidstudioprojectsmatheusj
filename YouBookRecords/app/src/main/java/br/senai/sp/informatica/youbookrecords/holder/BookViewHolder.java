package br.senai.sp.informatica.youbookrecords.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.senai.sp.informatica.youbookrecords.BookFormActivity;
import br.senai.sp.informatica.youbookrecords.R;
import br.senai.sp.informatica.youbookrecords.adapter.BookAdapter;
import br.senai.sp.informatica.youbookrecords.model.Book;

public class BookViewHolder extends RecyclerView.ViewHolder{

    private TextView nameField;
    private TextView authorField;
//    private TextView genreField;
//    private TextView publisherField;
//    private TextView publishedDateField;
    private Long bookId;

    public BookViewHolder(View iV, BookAdapter adapter) {
        super(iV);

        nameField = iV.findViewById(R.id.item_name);
        authorField = iV.findViewById(R.id.item_author);
//        genreField = iV.findViewById(R.id.item_genre);
//        publisherField = iV.findViewById(R.id.item_publisher);
//        publishedDateField = iV.findViewById(R.id.item_publishedDate);
        iV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Activity context = (Activity) v.getContext();
                final Intent intent = new Intent(context, BookFormActivity.class);
                intent.putExtra("bookId", bookId);
                context.startActivityForResult(intent, 1);
            }
        });
    }

    public void fill(Book book) {
        bookId = book.getId();
        nameField.setText(book.getName());
        authorField.setText(book.getAuthor());
//        publisherField.setText(book.getPublisher());
//        publishedDateField.setText(book.getPublishedDate());
    }
}
