package br.senai.sp.informatica.calculodejuros;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button botaoCalcular;
    private Button botaoLimpar;
    private EditText campoCapital;
    private EditText campoMeses;
    private EditText campoTaxa;
    private EditText campoResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Buscar as referências
          botaoCalcular = findViewById(R.id.btnCalcular);
          botaoLimpar = findViewById(R.id.btnLimpar);
          campoCapital = findViewById(R.id.edCapital);
          campoMeses = findViewById(R.id.edMeses);
          campoTaxa = findViewById(R.id.edTaxa);
          campoResultado = findViewById(R.id.edResultado);

          botaoCalcular.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  try {
                      //Convertendo os valores
                      double capital = Double.parseDouble(campoCapital.getText().toString());
                      int meses = Integer.parseInt(campoMeses.getText().toString());
                      double taxa = Double.parseDouble(campoTaxa.getText().toString());

                      //Calculando o resultado
                      double resultado = ((capital * taxa / 100) * meses) + capital;

                      //Apresentando o resultado
                      campoResultado.setText(String.format("%,.2f", resultado));
                  }catch(Exception e){
                      Toast.makeText(getApplicationContext(),"Something went wrong. Invalid numbers", Toast.LENGTH_LONG).show();
                  }//Fim try/catch
              }//Fim onClick
          });

    }//Fim onCreate

    public void limparCampos(View view){
        campoCapital.setText("");
        campoMeses.setText("");
        campoTaxa.setText("");
        campoResultado.setText("");
        Toast.makeText(getApplicationContext(), "All fields were cleared", Toast.LENGTH_LONG).show();

    }

}