package br.senai.sp.informatica.cnpjconsulta.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CNPJ {

    private String nome;
    private String situacao;

    @Override
    public String toString() {
        return "CNPJ{" +
                "nome= " + nome + '\n' +
                "situacao= " + situacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
}