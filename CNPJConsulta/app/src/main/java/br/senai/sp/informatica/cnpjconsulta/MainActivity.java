package br.senai.sp.informatica.cnpjconsulta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import br.senai.sp.informatica.cnpjconsulta.model.CNPJ;
import br.senai.sp.informatica.cnpjconsulta.service.HttpService;

public class MainActivity extends AppCompatActivity {

    private EditText etCnpj;
    private TextView tVResultado;
    private Button btnConsulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCnpj = findViewById(R.id.editCnpj);
        tVResultado = findViewById(R.id.resultadoId);
        btnConsulta = findViewById(R.id.btnConsultar);

        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CNPJ cnpjRetornado = new HttpService(etCnpj.getText().toString()).execute().get();
                    tVResultado.setText(cnpjRetornado.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
