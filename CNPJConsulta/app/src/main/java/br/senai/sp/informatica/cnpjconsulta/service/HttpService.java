package br.senai.sp.informatica.cnpjconsulta.service;

import android.os.AsyncTask;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;

import br.senai.sp.informatica.cnpjconsulta.model.CNPJ;
import br.senai.sp.informatica.cnpjconsulta.utils.AppUtils;

public class HttpService extends AsyncTask<Void, Void, CNPJ> {
    private final String cnpj;

    public HttpService(String cnpj) {
        this.cnpj = cnpj;
    }

    @Override
    protected CNPJ doInBackground(Void... voids) {

        StringBuilder resposta = new StringBuilder(); // Armazenará todas as Strings que vierem do Scanner
        if (cnpj != null && cnpj.length() == 8) {
            try {
                URL url = new URL(AppUtils.BASE_URL + cnpj);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setConnectTimeout(5000);
                connection.setDoOutput(true); // Permite a saída (envio de dados), acima estamos apenas recebendo
                connection.connect();

                Scanner scanner = new Scanner(url.openStream());
                while (scanner.hasNext()) {
                    resposta.append(scanner.next());
                }
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try {
            return objectMapper.readValue(resposta.toString(), CNPJ.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}