package br.senai.sp.informatica.elementosdetela;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CheckBox optDragon;
    private CheckBox optPhoenix;
    private Button btnVote;
    private TextView result;
    private AlertDialog.Builder dialog;
    private ImageView imgResp;
    private SeekBar seekDragon;
    private SeekBar seekPhoenix;
    private TextView textDragonID;
    private TextView textPhoenixID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        optDragon = findViewById(R.id.cbDragon);
        optPhoenix = findViewById(R.id.cbPhoenix);
        btnVote = findViewById(R.id.btnVote);
        result = findViewById(R.id.mcChoice);
        imgResp = findViewById(R.id.imgSub);
        seekDragon = findViewById(R.id.sbDragon);
        seekPhoenix = findViewById(R.id.sbPhoenix);
        textDragonID = findViewById(R.id.textDragon);
        textPhoenixID = findViewById(R.id.textPhoenix);

        //Configurando a SeekBar
        seekDragon.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textDragonID.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekPhoenix.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textPhoenixID.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        btnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectableItems = "";
                if (optDragon.isChecked()) {
                    selectableItems = "Item: " + optDragon.getText();
                    result.setText(selectableItems);
                } else if (optPhoenix.isChecked()) {
                    selectableItems = "Item: " + optPhoenix.getText();
                    result.setText(selectableItems);
                } else {
                    selectableItems = "No choice was made";
                    result.setText(selectableItems);
                }
                if ((optDragon.isChecked()) && (optPhoenix.isChecked())) {
                    selectableItems = "Items: " + optDragon.getText() + " & " + optPhoenix.getText();
                    result.setText(selectableItems);
                }//Fim dos IFs

                if (!(optDragon.isChecked()) && !(optPhoenix.isChecked())) {
                    imgResp.setImageResource(R.drawable.dragonxphoenix);
                } else {
                    dialog = new AlertDialog.Builder(MainActivity.this);
                    //Configurando o Título
                    dialog.setTitle("Warning!");
                    //Configurando a mensagem
                    dialog.setMessage("Do you really like these creatures?\n" + selectableItems);
                    //Configurando botão sim
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(getApplicationContext(), "Button yes was pressed", Toast.LENGTH_SHORT).show();
                            if (optDragon.isChecked()) {
                                imgResp.setImageResource(R.drawable.dragon);
                            }
                            if (optPhoenix.isChecked()) {
                                imgResp.setImageResource(R.drawable.phoenix);
                            }
                            if ((optDragon.isChecked()) && (optPhoenix.isChecked())) {
                                imgResp.setImageResource(R.drawable.bothmc);
                            }
//                            if (!(optDragon.isChecked()) && !(optPhoenix.isChecked())) {
//                                imgResp.setImageResource(R.drawable.dragonxphoenix);
//                            }
                        }
                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(getApplicationContext(), "Button no was pressed", Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog.setIcon(R.drawable.icondp);
                    dialog.create();
                    dialog.show();
                }//Fim do onClick
            }
        });

    }
}
