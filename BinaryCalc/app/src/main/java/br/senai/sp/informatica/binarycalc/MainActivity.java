package br.senai.sp.informatica.binarycalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private EditText numberField;
    private Button btnCalc;
    private Button btnClean;
    private TextView resultField;
    private ToggleButton toggleConversion;
    private String inputString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberField = findViewById(R.id.ptNumId);
        btnCalc = findViewById(R.id.btnCalcId);
        btnClean = findViewById(R.id.btcCleanId);
        resultField = findViewById(R.id.tvResultId);
        toggleConversion = findViewById(R.id.btnConvertId);

        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputString = numberField.getText().toString();
                if (numberField.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Field is empty", Toast.LENGTH_SHORT).show();
                } else {
                    Integer binary = Integer.parseInt(numberField.getText().toString());
                    String resultado = String.valueOf(binary);
                    String result = (Integer.toString(Integer.parseInt(resultado, 2), 10));
                    resultField.setText(result);
                }
            }
        });

        toggleConversion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    btnCalc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (numberField.getText().toString().equals("")) {
                                Toast.makeText(MainActivity.this, "Field is empty", Toast.LENGTH_SHORT).show();
                            } else {
                                Integer decimal = Integer.parseInt(numberField.getText().toString());
                                String resultado = String.valueOf(decimal);
                                String result = (Integer.toString(Integer.parseInt(resultado, 10), 2));
                                resultField.setText(result);
                            }
                        }
                    });
                }else{
                    btnCalc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (numberField.getText().toString().equals("")) {
                                Toast.makeText(MainActivity.this, "Field is empty", Toast.LENGTH_SHORT).show();
                            } else {
                                Integer binary = Integer.parseInt(numberField.getText().toString());
                                String resultado = String.valueOf(binary);
                                String result = (Integer.toString(Integer.parseInt(resultado, 2), 10));
                                resultField.setText(result);
                            }
                        }
                    });
                }
            }
        });

    }

    public void cleanFields(View view) {
        numberField.setText("");
        resultField.setText("Result");
        Toast.makeText(this, "All fields were cleaned", Toast.LENGTH_SHORT).show();
    }
}
