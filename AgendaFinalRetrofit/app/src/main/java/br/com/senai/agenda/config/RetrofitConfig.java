package br.com.senai.agenda.config;

import com.google.gson.Gson;

import br.com.senai.agenda.service.ContatoService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.senai.agenda.utils.AppUtils.CONTATO_BASE_URL;

public class RetrofitConfig {

    private final Retrofit retrofit;

    public RetrofitConfig() {
        retrofit = new Retrofit.Builder().baseUrl(CONTATO_BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }

    // Classe que vai acessar a interface (que contém os métodos [GET, POST, PUT, DELETE])
    public ContatoService getContatoInterface(){
        return retrofit.create(ContatoService.class);
    }

}