package br.com.senai.agenda.service;

import java.util.List;

import br.com.senai.agenda.modelo.Contato;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContatoService {
    @GET("api/contatos")
    Call<List<Contato>> listarContatos(); // Como a idéia é apenas para listar, não recebemos parâmetro algum

    @POST("api/contatos")
    Call<List<Contato>> salvarContato(@Body Contato contato); // O post tem como response (retorno) uma lista, então deve-se passar uma lista para evitar erros

    @GET("api/contatos/{id}")
    Call<Contato> listarContato(@Path("id") Long id);

    @PUT("api/contatos/{id}")
    Call<Void> editarContato(@Path("id") Long id, @Body Contato contato); // Passaremos um parâmetro(para editar) e um objeto (que é editado)
    // Já que o PUT não retorna nada, podemos deixar Void

    @DELETE("api/contatos/{id}")
    Call<Void> deletarContato(@Path("id") Long id);
}
