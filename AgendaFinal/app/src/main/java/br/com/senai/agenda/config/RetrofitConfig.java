package br.com.senai.agenda.config;

import br.com.senai.agenda.service.ContatoService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.senai.agenda.util.AppUtil.BASE_URL;

public class RetrofitConfig {
    private Retrofit retrofit;
    public RetrofitConfig() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
    }
    public ContatoService getContatoService(){
        return retrofit.create(ContatoService.class);
    }
}