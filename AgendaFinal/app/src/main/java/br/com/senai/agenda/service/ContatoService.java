package br.com.senai.agenda.service;

import br.com.senai.agenda.modelo.Contato;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ContatoService {
    @POST("api/contatos") // chaves {} apenas quando estamos enviando algum parâmetro
    Call<Contato> salvarContato(@Body Contato contato);
}