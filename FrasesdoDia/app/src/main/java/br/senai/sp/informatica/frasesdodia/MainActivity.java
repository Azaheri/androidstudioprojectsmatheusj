package br.senai.sp.informatica.frasesdodia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button btnPrhase;
    private TextView newPrhase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPrhase = findViewById(R.id.btnFrase);
        newPrhase = findViewById(R.id.idFrase);



    }

    private String[] frases ={"Você não é tão ruim quanto os outros falam, é pior :)",
            "Se seu dia está ruim, tenha calma, ele pode piorar :)",
            "Cuidado para não ser atropelado hoje :)",
            "O Brasil nunca vai ser um país de primeiro mundo :)",
            "O Real sempre vai ser mais desvalorizado que o dollar :)",
            "Você mora no Brasil :)",
            "Você está na Santa Cecília :)",
            "Não ter dinheiro é ruim :)",
            "As coisas custam muito caro :)",
            "Pular de um prédio pode não ser saudável :)",
            "Boatos que este aplicativo diz apenas verdades :)",
            "Fato: Não conseguimos mastigar aço :)",
            "Já pensou que a cada dia, você fica mais velho? :)",
            "Bater a cabeça não é legal :)",
            "Steam sales só te deixam mais pobres :)",
            "Eeeeeee Macarena"};

    public void newPs(View view) {
        Random fraseRandom = new Random();
        int numeroAleatorio = fraseRandom.nextInt(frases.length);
        newPrhase.setText(frases[numeroAleatorio]);
    }
}