package br.senai.sp.informatica.somdosbichos;

import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView dog;
    private ImageView cat;
    private ImageView lion;
    private ImageView monkey;
    private ImageView sheep;
    private ImageView cow;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dog = findViewById(R.id.caoId);
        cat = findViewById(R.id.gatoId);
        lion = findViewById(R.id.leaoId);
        monkey = findViewById(R.id.macacoId);
        sheep = findViewById(R.id.ovelhaId);
        cow = findViewById(R.id.vacaId);

        dog.setOnClickListener(this);
        cat.setOnClickListener(this);
        lion.setOnClickListener(this);
        monkey.setOnClickListener(this);
        sheep.setOnClickListener(this);
        cow.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

//        Toast.makeText(getApplicationContext(), "Element selected" + view.getId(), Toast.LENGTH_SHORT).show();
        switch (view.getId()){
            case R.id.caoId:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                }
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.cao);
                playSound();
                break;
            case R.id.gatoId:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                }
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.gato);
                playSound();
                break;
            case R.id.leaoId:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                }
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.leao);
                playSound();
                break;
            case R.id.macacoId:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                }
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.macaco);
                playSound();
                break;
            case R.id.ovelhaId:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                }
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.ovelha);
                playSound();
                break;
            case R.id.vacaId:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                }
                mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.vaca);
                playSound();
                break;
        }

    }//fim onClick
    public void playSound(){
        if (mediaPlayer != null){
            mediaPlayer.start();
        }
    }

    @Override
    protected void onDestroy() {
        if (mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }
}
