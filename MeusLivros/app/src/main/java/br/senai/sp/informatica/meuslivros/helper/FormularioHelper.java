package br.senai.sp.informatica.meuslivros.helper;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import br.senai.sp.informatica.meuslivros.FormularioActivity;
import br.senai.sp.informatica.meuslivros.R;
import br.senai.sp.informatica.meuslivros.model.Livro;

public class FormularioHelper {

    private ImageView capaDoLivro;
    private EditText tituloDoLivro;
    private EditText autorDoLivro;
    private Button botaoCadastrar;
    private Livro livro;

    public ImageView getCapaDoLivro() {
        return capaDoLivro;
    }

    public void setCapaDoLivro(ImageView capaDoLivro) {
        this.capaDoLivro = capaDoLivro;
    }

    public Button getBotaoCadastrar() {
        return botaoCadastrar;
    }

    public void setBotaoCadastrar(Button botaoCadastrar) {
        this.botaoCadastrar = botaoCadastrar;
    }

    public FormularioHelper(FormularioActivity fA) {
         capaDoLivro = fA.findViewById(R.id.imgLivro);
         tituloDoLivro = fA.findViewById(R.id.editTitulo);
         autorDoLivro = fA.findViewById(R.id.editAutor);
         botaoCadastrar = fA.findViewById(R.id.btnCadastrar);

         livro = new Livro();
    }

    public Livro pegaLivro() {
        livro.setCaminhoDaCapa(capaDoLivro.getTag().toString());
        livro.setTituloDoLivro(tituloDoLivro.getText().toString());
        livro.setAutorDoLivro(autorDoLivro.getText().toString());
        return livro;
    }
}
