package br.senai.sp.informatica.meuslivros.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.senai.sp.informatica.meuslivros.R;
import br.senai.sp.informatica.meuslivros.adapter.holder.LivroHolder;
import br.senai.sp.informatica.meuslivros.model.Livro;

public class LivroAdapter extends RecyclerView.Adapter{

    private final List<Livro> livros;
    private final Context context;

    public LivroAdapter(Context context, List<Livro> livros) {
        this.context = context;
        this.livros = livros;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.lista_item_livro, parent, false);
        LivroHolder holder = new LivroHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        LivroHolder nossoHolder = (LivroHolder) holder;
        Livro livro = livros.get(position);
        nossoHolder.preencher(livro);
    }

    @Override
    public int getItemCount() {
        return livros.size();
    }
}
