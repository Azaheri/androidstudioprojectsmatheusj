package br.senai.sp.informatica.meuslivros.adapter.holder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.senai.sp.informatica.meuslivros.R;
import br.senai.sp.informatica.meuslivros.adapter.LivroAdapter;
import br.senai.sp.informatica.meuslivros.model.Livro;

public class LivroHolder extends RecyclerView.ViewHolder{

    private final LivroAdapter adapter;
    private ImageView capaDoLivro;
    private TextView tituloDoLivro;
    private TextView autorDoLivro;
    private Long livroId;

    public LivroHolder(View itemView, LivroAdapter adapter) {
        super(itemView);
        this.adapter = adapter;

        capaDoLivro = itemView.findViewById(R.id.item_capa);
        tituloDoLivro = itemView.findViewById(R.id.item_titulo);
        autorDoLivro = itemView.findViewById(R.id.item_autor);
    }

    public void preencher(Livro livro) {
        livroId = livro.getId();
        tituloDoLivro.setText(livro.getTituloDoLivro());
        autorDoLivro.setText(livro.getAutorDoLivro());
        Bitmap capaRetornada = BitmapFactory.decodeFile(livro.getCaminhoDaCapa());
        capaDoLivro.setImageBitmap(capaRetornada);
    }
}
