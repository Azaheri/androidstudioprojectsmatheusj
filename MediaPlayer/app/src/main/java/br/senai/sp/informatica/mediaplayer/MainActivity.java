package br.senai.sp.informatica.mediaplayer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnPlaySong;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPlaySong = findViewById(R.id.btnPlay);
        mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.musica);

        btnPlaySong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()){
                    pauseSong();
                }else {
                    playSong();
                }
            }//fim onClick
        });//fim onClickListener
    }//fim onCreate

    private void playSong(){
        if (mediaPlayer != null){
            mediaPlayer.start();
            btnPlaySong.setText(R.string.pause);
        }
    }//fim playSong

    private void pauseSong(){
        if (mediaPlayer != null){
            mediaPlayer.pause();
            btnPlaySong.setText(R.string.play);
        }
    }//fim pauseSong
}
