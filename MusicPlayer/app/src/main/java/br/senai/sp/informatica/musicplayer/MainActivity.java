package br.senai.sp.informatica.musicplayer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{

    private ToggleButton btn1;
    private ToggleButton btn2;
    private ToggleButton btn3;
    private SeekBar vol;
    private MediaPlayer mediaPlayer1 = new MediaPlayer();
    private MediaPlayer mediaPlayer2 = new MediaPlayer();
    private MediaPlayer mediaPlayer3 = new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = findViewById(R.id.btnPlay1);
        btn2 = findViewById(R.id.btnPlay2);
        btn3 = findViewById(R.id.btnPlay3);
        vol = findViewById(R.id.seekVolId);

        btn1.setOnCheckedChangeListener(this);
        btn2.setOnCheckedChangeListener(this);
        btn3.setOnCheckedChangeListener(this);
        mediaPlayer1 = MediaPlayer.create(MainActivity.this, R.raw.ieaiaio);
        mediaPlayer2 = MediaPlayer.create(MainActivity.this, R.raw.battlefield);
        mediaPlayer3 = MediaPlayer.create(MainActivity.this, R.raw.impossible);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.btnPlay1:
                if (mediaPlayer1.isPlaying()){
                    mediaPlayer1.pause();
                }else {
                    playSong1();
                }
                break;
            case R.id.btnPlay2:
                if (mediaPlayer2.isPlaying()){
                    mediaPlayer2.pause();
                }else {
                    playSong2();
                }
                break;
            case R.id.btnPlay3:
                if (mediaPlayer3.isPlaying()){
                    mediaPlayer3.pause();
                }else {
                    playSong3();
                }
                break;
        }

    }
    public void playSong1(){
        if (mediaPlayer1 != null){
            mediaPlayer1.start();
        }
    }
    public void playSong2(){
        if (mediaPlayer2 != null){
            mediaPlayer2.start();
        }
    }
    public void playSong3(){
        if (mediaPlayer3 != null){
            mediaPlayer3.start();
        }
    }



}
