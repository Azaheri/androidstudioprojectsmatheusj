package br.senai.sp.informatica.junkenpo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class powersScreenActivity extends AppCompatActivity {

    private Button returnScreen;
    private TextView powerPc;
    private TextView powerUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_powers_screen);

        returnScreen = findViewById(R.id.btnReturnId);
        powerPc = findViewById(R.id.powerId);
        powerUser = findViewById(R.id.powerId2);

        Bundle extra = getIntent().getExtras();
        if (extra != null){
            String powerTextPc = extra.getString("gogoPc");
            powerPc.setText(powerTextPc);
            String powerTextUser = extra.getString("gogoUser");
            powerUser.setText(powerTextUser);
        }

        returnScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(powersScreenActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
