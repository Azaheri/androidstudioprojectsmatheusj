package br.senai.sp.informatica.junkenpo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button btnPlay;
    private String[] power = {"Pedra", "Papel", "Tesoura"};
    private RadioGroup choices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlay = findViewById(R.id.btnPlayId);
        choices = findViewById(R.id.radioGId);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, powersScreenActivity.class);
                Random junkenPower = new Random();
                int randomPower = junkenPower.nextInt(power.length);
                intent.putExtra("gogoPc", power[randomPower]);
                //Log.e("sadf", String.valueOf(power[randomPower]));
                Integer passaId = checkChoices(choices);
                if (passaId != null) {
                    intent.putExtra("gogoUser", power[checkChoices(choices)]);
//                    Log.e("sadf", String.valueOf(choices));
                startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "No choices were made", Toast.LENGTH_SHORT).show();
                }
//                Log.e("sadf", String.valueOf(passaId));
            }
        });
    }

    private Integer checkChoices(RadioGroup rg) {
        Integer resultado = null;
        switch (rg.getCheckedRadioButtonId()) {
            case R.id.radioPedra:
                resultado = 0;
                break;
            case R.id.radioPapel:
                resultado = 1;
                break;
            case R.id.radioTesoura:
                resultado = 2;
                break;
        }
        return resultado;
    }
}
