package br.senai.sp.informatica.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HistoryActivity extends AppCompatActivity {

    private Button menu;
    private TextView history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        menu = findViewById(R.id.btnMenu);
        history = findViewById(R.id.tVHistory);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HistoryActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });

        history.setMovementMethod(new ScrollingMovementMethod());
    }
}
