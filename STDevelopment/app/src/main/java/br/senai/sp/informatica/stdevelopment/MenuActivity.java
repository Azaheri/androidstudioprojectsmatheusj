package br.senai.sp.informatica.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    private Button information;
    private Button history;
    private Button contact;
    private Button contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        information = findViewById(R.id.btnInfo);
        history = findViewById(R.id.btnHistory);
        contact = findViewById(R.id.btnContact);
        contactList = findViewById(R.id.btnList);

        information.setOnClickListener(new View.OnClickListener() { //Há também a possibilidade de implementar o método para o onclick para economizar código :)
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, InformationActivity.class);
                startActivity(intent);
                finish();
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, HistoryActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
