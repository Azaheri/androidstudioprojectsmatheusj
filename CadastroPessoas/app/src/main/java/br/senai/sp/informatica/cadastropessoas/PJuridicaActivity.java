package br.senai.sp.informatica.cadastropessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PJuridicaActivity extends AppCompatActivity {

    private EditText nomeU;
    private Button salvar;
    private JuridicaHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pjuridica);

        nomeU = findViewById(R.id.eTNameUser);
        salvar = findViewById(R.id.btnCadJuridica);
        helper = new JuridicaHelper(this);

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            String nome = extra.getString("nome");
            nomeU.setText(nome);
        }

        Intent intent = getIntent();
        PessoaJuridica juridica = (PessoaJuridica) intent.getSerializableExtra("juridica");
        if(juridica != null){
            helper.fillJuridicaForm(juridica);
        }

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PessoaDAOJuridica dao = new PessoaDAOJuridica(PJuridicaActivity.this);
                PessoaJuridica juridica = helper.pegarPessoaJ();
                if (juridica.getId() != null){
                    dao.update(juridica);
                }else {
                    dao.insereJuridica(juridica);
                }
                dao.close();
                Toast.makeText(PJuridicaActivity.this, juridica.getNome() + " - adicionado(a) com sucesso", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
