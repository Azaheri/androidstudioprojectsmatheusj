package br.senai.sp.informatica.cadastropessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class ListaJuridicaActivity extends AppCompatActivity {

    private ListView juridicasList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_juridica);
        juridicasList = findViewById(R.id.listaJuridica);
        registerForContextMenu(juridicasList);
        carregarListaJuridica();
        juridicasList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PessoaJuridica juridica = (PessoaJuridica) juridicasList.getItemAtPosition(i);
                Intent intent = new Intent(ListaJuridicaActivity.this, PJuridicaActivity.class);
                intent.putExtra("juridica", juridica);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem remover = menu.add("Remover");
        remover.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                PessoaJuridica juridica = (PessoaJuridica) juridicasList.getItemAtPosition(info.position);
                PessoaDAOJuridica dao = new PessoaDAOJuridica(ListaJuridicaActivity.this);
                dao.remover(juridica);
                dao.close();
                Toast.makeText(ListaJuridicaActivity.this, "Usuário: "+juridica.getNome()+" foi removido com sucesso.", Toast.LENGTH_SHORT).show();
                carregarListaJuridica();
//                juridicasList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                        PessoaJuridica juridica = (PessoaJuridica) juridicasList.getItemAtPosition(i);
//                        Intent intent = new Intent(ListaJuridicaActivity.this, PJuridicaActivity.class);
//                        intent.putExtra("juridica", juridica);
//                        startActivity(intent);
//
//                    }
//                });
                return false;
            }
        });
    }

    private void carregarListaJuridica(){
        PessoaDAOJuridica dao = new PessoaDAOJuridica(this);
        List<PessoaJuridica> juridicas = dao.procurarJuridica();
        ArrayAdapter<PessoaJuridica> adapter = new ArrayAdapter<PessoaJuridica>(this, android.R.layout.simple_list_item_1, juridicas);
        juridicasList.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregarListaJuridica();
    }
}
