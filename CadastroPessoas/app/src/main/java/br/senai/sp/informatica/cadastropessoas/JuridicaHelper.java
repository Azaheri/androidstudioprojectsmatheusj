package br.senai.sp.informatica.cadastropessoas;

import android.widget.EditText;

/**
 * Created by 39270271897 on 23/02/2018.
 */

public class JuridicaHelper {

    private EditText nomeU;
    private EditText sobrenome;
    private EditText email;
    private EditText cnpj;
    private EditText celular;
    private EditText bairro;
    private EditText rua;
    private EditText senha;
    private PessoaJuridica juridica;

    public JuridicaHelper(PJuridicaActivity cad){
        nomeU = cad.findViewById(R.id.eTNameUser);
        sobrenome = cad.findViewById(R.id.eTSobrenome);
        email = cad.findViewById(R.id.eTEmail);
        cnpj = cad.findViewById(R.id.eTCNPJ);
        celular = cad.findViewById(R.id.eTCelular);
        bairro = cad.findViewById(R.id.eTBairro);
        rua = cad.findViewById(R.id.eTRua);
        senha = cad.findViewById(R.id.eTSenha);
        juridica = new PessoaJuridica();
    }

    public PessoaJuridica pegarPessoaJ(){

        juridica.setNome(nomeU.getText().toString());
        juridica.setSobrenome(sobrenome.getText().toString());
        juridica.setEmail(email.getText().toString());
        juridica.setCpnj(cnpj.getText().toString());
        juridica.setCelular(celular.getText().toString());
        juridica.setBairro(bairro.getText().toString());
        juridica.setRua(rua.getText().toString());
        juridica.setSenha(senha.getText().toString());
        return juridica;
    }

    public void fillJuridicaForm(PessoaJuridica juridica){
        nomeU.setText(juridica.getNome());
        sobrenome.setText(juridica.getSobrenome());
        email.setText(juridica.getEmail());
        cnpj.setText(juridica.getCpnj());
        celular.setText(juridica.getCelular());
        bairro.setText(juridica.getBairro());
        rua.setText(juridica.getRua());
        senha.setText(juridica.getSenha());
        this.juridica = juridica;
    }
}
