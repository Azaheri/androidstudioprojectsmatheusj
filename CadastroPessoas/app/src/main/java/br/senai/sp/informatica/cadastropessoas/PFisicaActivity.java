package br.senai.sp.informatica.cadastropessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PFisicaActivity extends AppCompatActivity {

    private EditText nomeU;
    private Button salvar;
    private FisicaHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pfisica);

        nomeU = findViewById(R.id.eTNameUser);
        salvar = findViewById(R.id.btnCadFisica);
        helper = new FisicaHelper(this);

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            String nome = extra.getString("nome");
            nomeU.setText(nome);
        }

        Intent intent = getIntent();
        PessoaFisica fisica = (PessoaFisica) intent.getSerializableExtra("fisica");
        if (fisica != null){
            helper.fillFisicaForm(fisica);
        }

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PessoaDAOFisica dao = new PessoaDAOFisica(PFisicaActivity.this);
                PessoaFisica fisica = helper.pegarPessoaF();
                if (fisica.getId() != null){
                    dao.update(fisica);
                }else{
                    dao.insereFisica(fisica);
                }
                dao.close();
                Toast.makeText(PFisicaActivity.this, fisica.getNome() + " - adicionado(a) com sucesso", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}
