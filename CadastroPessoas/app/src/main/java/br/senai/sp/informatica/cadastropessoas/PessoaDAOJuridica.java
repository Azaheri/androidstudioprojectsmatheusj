package br.senai.sp.informatica.cadastropessoas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 39270271897 on 23/02/2018.
 */

public class PessoaDAOJuridica extends SQLiteOpenHelper{
    public PessoaDAOJuridica(Context context) {
        super(context, "CadastroJuridicas", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql2 = "CREATE TABLE PessoaJuridica(id INTEGER PRIMARY KEY, nome TEXT NOT NULL, sobrenome TEXT NOT NULL, email TEXT NOT NULL, cnpj TEXT NOT NULL, celular TEXT NOT NULL, bairro TEXT NOT NULL, rua TEXT NOT NULL, senha TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql2 = "DROP TABLE PessoaJuridica";
        sqLiteDatabase.execSQL(sql2);
    }

    public void insereJuridica(PessoaJuridica pessoaJ){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = getContentValues(pessoaJ);
        db.insert("PessoaJuridica", null, dados);
    }

    @NonNull
    private ContentValues getContentValues(PessoaJuridica pessoaJ) {
        ContentValues dados = new ContentValues();
        dados.put("nome", pessoaJ.getNome());
        dados.put("sobrenome", pessoaJ.getSobrenome());
        dados.put("email", pessoaJ.getEmail());
        dados.put("cnpj", pessoaJ.getCpnj());
        dados.put("celular", pessoaJ.getCelular());
        dados.put("bairro", pessoaJ.getBairro());
        dados.put("rua", pessoaJ.getRua());
        dados.put("senha", pessoaJ.getSenha());
        return dados;
    }

    public List<PessoaJuridica> procurarJuridica(){
        String sql2 = "SELECT * FROM PessoaJuridica";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql2, null);
        List<PessoaJuridica> juridicas = new ArrayList<>();
        while (c.moveToNext()){
            PessoaJuridica pessoaJ = new PessoaJuridica();
            pessoaJ.setId(c.getLong(c.getColumnIndex("id")));
            pessoaJ.setNome(c.getString(c.getColumnIndex("nome")));
            pessoaJ.setSobrenome(c.getString(c.getColumnIndex("sobrenome")));
            pessoaJ.setEmail(c.getString(c.getColumnIndex("email")));
            pessoaJ.setCpnj(c.getString(c.getColumnIndex("cnpj")));
            pessoaJ.setCelular(c.getString(c.getColumnIndex("celular")));
            pessoaJ.setBairro(c.getString(c.getColumnIndex("bairro")));
            pessoaJ.setRua(c.getString(c.getColumnIndex("rua")));
            pessoaJ.setSenha(c.getString(c.getColumnIndex("senha")));

            juridicas.add(pessoaJ);
        }
        return juridicas;
    }

    public void remover(PessoaJuridica juridica) {
        SQLiteDatabase db = getWritableDatabase();
        String[] parametros = {juridica.getId().toString()};
        db.delete("PessoaJuridica", "id = ?", parametros);
    }

    public void update(PessoaJuridica juridica){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = getContentValues(juridica);
        String[] param = {juridica.getId().toString()};
        db.update("PessoaJuridica", dados, "id = ?", param);
    }
}
