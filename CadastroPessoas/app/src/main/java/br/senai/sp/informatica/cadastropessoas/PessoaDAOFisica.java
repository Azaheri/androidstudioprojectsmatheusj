package br.senai.sp.informatica.cadastropessoas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 39270271897 on 23/02/2018.
 */

public class PessoaDAOFisica extends SQLiteOpenHelper {

    public PessoaDAOFisica(Context context) {
        super(context, "CadastroFisicas", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql1 = "CREATE TABLE PessoaFisica(id INTEGER PRIMARY KEY, nome TEXT NOT NULL, sobrenome TEXT NOT NULL, email TEXT NOT NULL, cpf TEXT NOT NULL, celular TEXT NOT NULL, bairro TEXT NOT NULL, rua TEXT NOT NULL, senha TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql1 = "DROP TABLE PessoaFisica";
        sqLiteDatabase.execSQL(sql1);
    }

    public void insereFisica(PessoaFisica pessoaF){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = getContentValues(pessoaF);
        db.insert("PessoaFisica", null, dados);
    }

    @NonNull
    private ContentValues getContentValues(PessoaFisica pessoaF) {
        ContentValues dados = new ContentValues();
        dados.put("nome", pessoaF.getNome());
        dados.put("sobrenome", pessoaF.getSobrenome());
        dados.put("email", pessoaF.getEmail());
        dados.put("cpf", pessoaF.getCpf());
        dados.put("celular", pessoaF.getCelular());
        dados.put("bairro", pessoaF.getBairro());
        dados.put("rua", pessoaF.getRua());
        dados.put("senha", pessoaF.getSenha());
        return dados;
    }

    public List<PessoaFisica> procurarFisica(){
        String sql1 = "SELECT * FROM PessoaFisica";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql1, null);
        List<PessoaFisica> fisicas = new ArrayList<>();
        while (c.moveToNext()){
            PessoaFisica pessoaF = new PessoaFisica();
            pessoaF.setId(c.getLong(c.getColumnIndex("id")));
            pessoaF.setNome(c.getString(c.getColumnIndex("nome")));
            pessoaF.setSobrenome(c.getString(c.getColumnIndex("sobrenome")));
            pessoaF.setEmail(c.getString(c.getColumnIndex("email")));
            pessoaF.setCpf(c.getString(c.getColumnIndex("cpf")));
            pessoaF.setCelular(c.getString(c.getColumnIndex("celular")));
            pessoaF.setBairro(c.getString(c.getColumnIndex("bairro")));
            pessoaF.setRua(c.getString(c.getColumnIndex("rua")));
            pessoaF.setSenha(c.getString(c.getColumnIndex("senha")));

            fisicas.add(pessoaF);
        }
        return fisicas;
    }

    public void remover(PessoaFisica fisica) {
        SQLiteDatabase db = getWritableDatabase();
        String[] parametros = {fisica.getId().toString()};
        db.delete("PessoaFisica", "id = ?", parametros);
    }

    public void update(PessoaFisica fisica){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = getContentValues(fisica);
        String[] param = {fisica.getId().toString()};
        db.update("PessoaFisica", dados, "id = ?", param);
    }
}
