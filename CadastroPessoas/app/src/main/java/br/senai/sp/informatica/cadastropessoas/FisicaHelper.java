package br.senai.sp.informatica.cadastropessoas;

import android.widget.EditText;

/**
 * Created by 39270271897 on 23/02/2018.
 */

public class FisicaHelper {

    private EditText nomeU;
    private EditText sobrenome;
    private EditText email;
    private EditText cpf;
    private EditText celular;
    private EditText bairro;
    private EditText rua;
    private EditText senha;
    private PessoaFisica fisica;

    public FisicaHelper(PFisicaActivity cad){
        nomeU = cad.findViewById(R.id.eTNameUser);
        sobrenome = cad.findViewById(R.id.eTSobrenome);
        email = cad.findViewById(R.id.eTEmail);
        cpf = cad.findViewById(R.id.eTCPF);
        celular = cad.findViewById(R.id.eTCelular);
        bairro = cad.findViewById(R.id.eTBairro);
        rua = cad.findViewById(R.id.eTRua);
        senha = cad.findViewById(R.id.eTSenha);
        fisica = new PessoaFisica();
    }

    public PessoaFisica pegarPessoaF(){

        fisica.setNome(nomeU.getText().toString());
        fisica.setSobrenome(sobrenome.getText().toString());
        fisica.setEmail(email.getText().toString());
        fisica.setCpf(cpf.getText().toString());
        fisica.setCelular(celular.getText().toString());
        fisica.setBairro(bairro.getText().toString());
        fisica.setRua(rua.getText().toString());
        fisica.setSenha(senha.getText().toString());
        return fisica;
    }

    public void fillFisicaForm(PessoaFisica fisica){
        nomeU.setText(fisica.getNome());
        sobrenome.setText(fisica.getSobrenome());
        email.setText(fisica.getEmail());
        cpf.setText(fisica.getCpf());
        celular.setText(fisica.getCelular());
        bairro.setText(fisica.getBairro());
        rua.setText(fisica.getRua());
        senha.setText(fisica.getSenha());
        this.fisica = fisica;
    }
}
