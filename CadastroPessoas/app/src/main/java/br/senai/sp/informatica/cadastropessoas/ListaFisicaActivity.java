package br.senai.sp.informatica.cadastropessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class ListaFisicaActivity extends AppCompatActivity {

    private ListView fisicasList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_fisica);
        fisicasList = findViewById(R.id.listaFisica);
        carregarListaFisica();
        fisicasList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PessoaFisica fisica = (PessoaFisica) fisicasList.getItemAtPosition(i);
                Intent intent = new Intent(ListaFisicaActivity.this, PFisicaActivity.class);
                intent.putExtra("fisica", fisica);
                startActivity(intent);

            }
        });
        registerForContextMenu(fisicasList);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem remover = menu.add("Remover");
        remover.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                PessoaFisica fisica = (PessoaFisica) fisicasList.getItemAtPosition(info.position);
                PessoaDAOFisica dao = new PessoaDAOFisica(ListaFisicaActivity.this);
                dao.remover(fisica);
                dao.close();
                Toast.makeText(ListaFisicaActivity.this, "Usuário: " + fisica.getNome() + " foi removido com sucesso.", Toast.LENGTH_SHORT).show();
                carregarListaFisica();
                return false;
            }
        });
    }

    private void carregarListaFisica(){
        PessoaDAOFisica dao = new PessoaDAOFisica(this);
        List<PessoaFisica> fisicas = dao.procurarFisica();
        ArrayAdapter<PessoaFisica> adapter = new ArrayAdapter<PessoaFisica>(this, android.R.layout.simple_list_item_1, fisicas);
        fisicasList.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregarListaFisica();
    }
}
