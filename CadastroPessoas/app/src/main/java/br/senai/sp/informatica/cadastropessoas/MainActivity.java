package br.senai.sp.informatica.cadastropessoas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText eTNome;
    private RadioGroup pessoas;
    private Button continuar;
    private Button listar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eTNome = findViewById(R.id.eTNameUser);
        pessoas = findViewById(R.id.radioGroup);
        continuar = findViewById(R.id.btnGoCad);
        listar = findViewById(R.id.btnListar);
        
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer passaId = checkRadio(pessoas);
                if (passaId == null){
                    Toast.makeText(MainActivity.this, "Erro: Tipo de pessoa não selecionada", Toast.LENGTH_SHORT).show();
                }else if(passaId == 0){
                    Intent intent = new Intent(MainActivity.this, PFisicaActivity.class);
                    intent.putExtra("nome", eTNome.getText().toString());
                    startActivity(intent);
                    eTNome.setText("");
                }else if(passaId == 1){
                    Intent intent = new Intent(MainActivity.this, PJuridicaActivity.class);
                    intent.putExtra("nome", eTNome.getText().toString());
                    startActivity(intent);
                    eTNome.setText("");
                }
            }
        });

        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer passaId = checkRadio(pessoas);
                if (passaId == null){
                    Toast.makeText(MainActivity.this, "Erro: Tipo de pessoa não selecionada", Toast.LENGTH_SHORT).show();
                }else if(passaId == 0){
                    Intent intent = new Intent(MainActivity.this, ListaFisicaActivity.class);
                    startActivity(intent);
                }else if(passaId == 1){
                    Intent intent = new Intent(MainActivity.this, ListaJuridicaActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    private Integer checkRadio(RadioGroup rg){
        Integer resultado = null;
        switch (rg.getCheckedRadioButtonId()){
            case R.id.rBFisica:
                resultado = 0;
                break;
            case R.id.rBJuridica:
                resultado = 1;
                break;
        }
        return resultado;
    }
}
