package br.com.senai.tablayouttarde.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.senai.tablayouttarde.fragments.ContatosFragment;
import br.com.senai.tablayouttarde.fragments.ConversasFragment;
import br.com.senai.tablayouttarde.fragments.DataFragment;

/**
 * Created by adminLocal on 18/04/2018.
 */

public class TabAdapter extends FragmentStatePagerAdapter{
    String[] titulosTabs = {"CONVERSAS","CONTATOS","DATA"};
    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
            Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new ConversasFragment();
                break;
            case 1:
                fragment = new ContatosFragment();
                break;
            case 2:
                fragment = new DataFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return titulosTabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titulosTabs[position];
    }
}
