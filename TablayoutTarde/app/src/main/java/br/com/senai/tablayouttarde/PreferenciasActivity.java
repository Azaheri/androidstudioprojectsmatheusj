package br.com.senai.tablayouttarde;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class PreferenciasActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButtonSelecionado;
    private Button botaoSalvar;
    private static final String ARQUIVO_PREFERENCIA = "arqPreferencia";
    private ConstraintLayout activityPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);


        radioGroup = findViewById(R.id.radioGroupID);
        botaoSalvar = findViewById(R.id.btnSalvar);
        activityPref = findViewById(R.id.activityPreferencias);
        
        //Recuperando Preferencias
        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if (sharedPreferences.contains("corSelecionada")){
            String corRecuperada = sharedPreferences.getString("corSelecionada", "Padrão");
            setPreferences(corRecuperada);
        }


        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idRadioButtonEscolhido = radioGroup.getCheckedRadioButtonId();

                if (idRadioButtonEscolhido>0) {
                    SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    radioButtonSelecionado = findViewById(idRadioButtonEscolhido);
                    String valorSelecionado = radioButtonSelecionado.getText().toString();
                    editor.putString("corSelecionada", valorSelecionado);
                    editor.commit();

                    setPreferences(valorSelecionado);
                }
            }
        });

    }

    private void setPreferences(String valorSelecionado) {
            switch (valorSelecionado){
                case "Azul":
                    activityPref.setBackgroundColor(Color.parseColor("#3F51B5"));
                    break;
                case "Roxo":
                    activityPref.setBackgroundColor(Color.parseColor("#0051B5"));
                    break;
                case "Amarelo":
                    activityPref.setBackgroundColor(Color.parseColor("#0051cc"));
                    break;
                case "Padrão":
                    activityPref.setBackgroundColor(Color.parseColor("#FF51cc"));
                    break;
            }
    }
}
