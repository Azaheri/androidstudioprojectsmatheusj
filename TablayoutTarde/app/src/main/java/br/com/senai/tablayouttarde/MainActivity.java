package br.com.senai.tablayouttarde;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import br.com.senai.tablayouttarde.adapter.TabAdapter;
import br.com.senai.tablayouttarde.helper.SlidingTabLayout;

public class MainActivity extends AppCompatActivity {

    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        slidingTabLayout = findViewById(R.id.stl_tabs);
        viewPager = findViewById(R.id.vp_pagina);

        //distibuindo os itens
        slidingTabLayout.setDistributeEvenly(true);

        //configurando adapter
        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabAdapter);

        slidingTabLayout.setViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.item_preferencias:
                Intent intent = new Intent(getApplicationContext(), PreferenciasActivity.class);
                startActivity(intent);
                break;
            case R.id.item_data:
                Intent intentData = new Intent(getApplicationContext(), TesteDataActivity.class);
                startActivity(intentData);

        }
        return super.onOptionsItemSelected(item);
    }
}
