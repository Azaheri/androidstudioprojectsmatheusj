package br.senai.sp.informatica.cepconsulta.service;

import android.os.AsyncTask;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import br.senai.sp.informatica.cepconsulta.model.CEP;
import br.senai.sp.informatica.cepconsulta.utils.AppUtils;

public class HttpService extends AsyncTask<Void, Void, CEP> {
    private final String cep;

    public HttpService(String cep) {
        this.cep = cep;
    }

    @Override
    protected CEP doInBackground(Void... voids) {

        StringBuilder resposta = new StringBuilder(); // Armazenará todas as Strings que vierem do Scanner
        if (cep != null && cep.length() == 8) {
            try {
                URL url = new URL(AppUtils.BASE_URL + cep);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setConnectTimeout(5000);
                connection.setDoOutput(true); // Permite a saída (envio de dados), acima estamos apenas recebendo
                connection.connect();

                Scanner scanner = new Scanner(url.openStream());
                while (scanner.hasNext()) {
                    resposta.append(scanner.next());
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.readValue(resposta.toString(), CEP.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}