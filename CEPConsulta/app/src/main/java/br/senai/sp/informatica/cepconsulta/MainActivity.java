package br.senai.sp.informatica.cepconsulta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import br.senai.sp.informatica.cepconsulta.config.RetrofitConfig;
import br.senai.sp.informatica.cepconsulta.model.CEP;
import br.senai.sp.informatica.cepconsulta.service.HttpService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private EditText etCep;
    private TextView tVResultado, textCidade, textEstado, textBairro, textLogradouro;
    private Button btnConsulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCep = findViewById(R.id.editCEP);
        tVResultado = findViewById(R.id.resultadoId);
        btnConsulta = findViewById(R.id.btnConsulta);
        textCidade = findViewById(R.id.tVCidade);
        textEstado = findViewById(R.id.tVEstado);
        textBairro = findViewById(R.id.tVBairro);
        textLogradouro = findViewById(R.id.tVLogradouro);

//        btnConsulta.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    CEP cepRetornado = new HttpService(etCep.getText().toString()).execute().get();
////                    tVResultado.setText(cepRetornado.toString());
//                    textEstado.setText(cepRetornado.getEstado());
//                    textCidade.setText(cepRetornado.getCidade());
//                    textBairro.setText(cepRetornado.getBairro());
//                    textLogradouro.setText(cepRetornado.getLogradouro());
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<CEP> call = new RetrofitConfig().getCEPService().buscarCEP(etCep.getText().toString());
                //call.execute(); <-- para Métodos SINCRONOS
                call.enqueue(new Callback<CEP>() { // <-- para Métodos ASSINCRONOS
                    @Override
                    public void onResponse(Call<CEP> call, Response<CEP> response) {
                        if (response.isSuccessful()){
                            CEP cep = response.body();
                            tVResultado.setText(cep.toString());
                            textCidade.setText(cep.getCidade());
                        }
                    }

                    @Override
                    public void onFailure(Call<CEP> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Não foi possível localizar o CEP", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}