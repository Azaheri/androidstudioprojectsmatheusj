package br.senai.sp.informatica.cepconsulta.service;

import br.senai.sp.informatica.cepconsulta.model.CEP;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

// É a interface que armazenará nossos métodos
public interface CEPService {
    // Colocar dentro dos parenteses o que irá ser recebido. (O que passaremos na URL)
    // Nossa BASE_URL já está definida, o get será o que vem após ela, poderíamos acessar outros diretórios através do get
    // e.g.: @GET("/algumDiretorioAqui{oQueVaiNaUrl}")
    @GET("{cep}")
    Call<CEP> buscarCEP(@Path("cep") String cep);

    // Com o método feito, precisaremos de alguém para acessar esse método, então configuraremos isso no RetrofitConfig
}
