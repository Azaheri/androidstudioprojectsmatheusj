package br.senai.sp.informatica.cepconsulta.config;

import br.senai.sp.informatica.cepconsulta.service.CEPService;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static br.senai.sp.informatica.cepconsulta.utils.AppUtils.BASE_URL;

public class RetrofitConfig {

    private Retrofit retrofit;

    public RetrofitConfig() {
        // Construindo um objeto da nossa aplicação
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                // Definindo uma base url
                .baseUrl(BASE_URL)
                // Convertendo Json para Java
                .addConverterFactory(JacksonConverterFactory.create()).build();
    }

    // Dessa maneira, é possível acessar qualquer método da interface CEPService (encapsulamento)
    public CEPService getCEPService(){
        return retrofit.create(CEPService.class);
    }
}