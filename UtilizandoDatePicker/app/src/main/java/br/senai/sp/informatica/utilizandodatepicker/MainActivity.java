package br.senai.sp.informatica.utilizandodatepicker;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private TextView dataEntrada, dataSaida, resultado;
    private Button botaoCalcular;
    private DatePickerDialog.OnDateSetListener dateListenerIn, dateListenerOut;
    private LocalDate entradaTratada, saidaTratada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataEntrada = findViewById(R.id.dataEntadaId);
        dataSaida = findViewById(R.id.dataSaidaId);
        resultado = findViewById(R.id.resultadoId);
        botaoCalcular = findViewById(R.id.botaoCalcular);

        dataEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendario = Calendar.getInstance();
                int year = calendario.get(Calendar.YEAR);
                int month = calendario.get(Calendar.MONTH);
                int day = calendario.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateListenerIn, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        }); // Fim do onclick

        dataSaida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendario = Calendar.getInstance();
                int year = calendario.get(Calendar.YEAR);
                int month = calendario.get(Calendar.MONTH);
                int day = calendario.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateListenerOut, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        }); // Fim do onclick

        dateListenerIn = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month +=1;
                String dataEntradaRetornada = dayOfMonth+"/"+month+"/"+year;
                entradaTratada = LocalDate.of(year, month, dayOfMonth);
                dataEntrada.setText(dataEntradaRetornada);
            }
        }; // Fim do onclick

        dateListenerOut = new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month +=1;
                String dataSaidaRetornada = dayOfMonth+"/"+month+"/"+year;
                saidaTratada = LocalDate.of(year, month, dayOfMonth);
                dataSaida.setText(dataSaidaRetornada);
            }
        }; // Fim do onclick

        botaoCalcular.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (entradaTratada == null || saidaTratada == null){
                    Toast.makeText(MainActivity.this, "Insira as datas corretamente", Toast.LENGTH_SHORT).show();
                }else {
                    Long diferencaData = entradaTratada.until(saidaTratada, ChronoUnit.DAYS);
                    resultado.setText(String.valueOf(diferencaData));
                }

            }
        });
    }
}
