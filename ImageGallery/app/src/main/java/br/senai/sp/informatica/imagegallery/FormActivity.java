package br.senai.sp.informatica.imagegallery;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import br.senai.sp.informatica.imagegallery.DAO.ImageDAO;
import br.senai.sp.informatica.imagegallery.model.Img;

public class FormActivity extends AppCompatActivity {

    public static final int PERMISSION_REQUEST = 1;
    public static final int GALLERY_CODE = 1;
    private Button btnGallery;
    private ImageView loadedImage;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        btnGallery = findViewById(R.id.btn_gallery);
        loadedImage = findViewById(R.id.loadedImg);
        btnSave = findViewById(R.id.btn_save);

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_CODE);
            }
        }); // Fim onClick

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Img img = new Img();
                img.setImagePath(loadedImage.getTag().toString());
                ImageDAO dao = new ImageDAO(FormActivity.this);
                dao.insert(img);
                finish();
            }
        }); // Fim onClick

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST); // Control Alt C -> converte alguma coisa em constante
            }
        }

    } // Fim onCreate

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_CODE) {
            Uri uri = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = getContentResolver().query(uri, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String imagePath = c.getString(columnIndex);
            c.close();
            Bitmap imgReturn = (BitmapFactory.decodeFile(imagePath));
            loadedImage.setImageBitmap(imgReturn);
            loadedImage.setTag(imagePath);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // / A permissão foi concedida. Pode continuar
            } else{
                // A permissão foi negada. Precisa ver o que deve ser desabilitado

            }
            return;
        }

    }
}
