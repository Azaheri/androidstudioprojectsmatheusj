package br.senai.sp.informatica.imagegallery.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.imagegallery.model.Img;

public class ImageDAO extends SQLiteOpenHelper{
    public ImageDAO(Context context) {
        super(context, "Image Gallery", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Images(id INTEGER PRIMARY KEY, imagePath TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Images";
        db.execSQL(sql);
    }

    public void insert(Img img) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues data = new ContentValues();
        data.put("imagePath", img.getImagePath());
        db.insert("Images", null, data);
    }

    public List<Img> searchImages() {
        String sql = "SELECT * FROM Images";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Img> images = new ArrayList<>();
        while(c.moveToNext()){
            Img img = new Img();
            img.setId(c.getLong(c.getColumnIndex("id")));
            img.setImagePath(c.getString(c.getColumnIndex("imagePath")));
            images.add(img);
        }
        c.close();
        return images;
    }
}
