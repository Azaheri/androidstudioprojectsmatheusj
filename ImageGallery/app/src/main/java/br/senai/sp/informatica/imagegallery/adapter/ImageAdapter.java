package br.senai.sp.informatica.imagegallery.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

import br.senai.sp.informatica.imagegallery.R;
import br.senai.sp.informatica.imagegallery.model.Img;

public class ImageAdapter extends BaseAdapter{

    private final Context context;
    private final List<Img> images;

    public ImageAdapter(Context context, List<Img> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return images.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Img img = images.get(position);
        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.lista_imagens, parent, false);
        ImageView listImage = view.findViewById(R.id.imageList);
        String imagePath = img.getImagePath();
        if (imagePath != null){
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            Bitmap reducedImage = Bitmap.createScaledBitmap(bitmap, 150, 150, true); // True para melhorar a qualidade (caso seja perdida com a redução)
            listImage.setImageBitmap(reducedImage);
        }
        return view;
    }

}
