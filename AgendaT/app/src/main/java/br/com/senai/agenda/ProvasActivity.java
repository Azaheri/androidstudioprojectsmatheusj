package br.com.senai.agenda;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import br.com.senai.agenda.modelo.ListaProvasFragments;
import br.com.senai.agenda.modelo.Prova;

public class ProvasActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragments_lista_provas);

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction tx =  fragmentManager.beginTransaction();
        tx.replace(R.id.frame_principal, new ListaProvasFragments());
        tx.commit();



    }
}
