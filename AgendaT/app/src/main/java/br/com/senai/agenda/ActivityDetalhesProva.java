package br.com.senai.agenda;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import br.com.senai.agenda.modelo.Prova;

public class ActivityDetalhesProva extends AppCompatActivity {

    private TextView materia;
    private TextView data;
    private ListView listaTopicos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_prova);

        Intent intent = getIntent();
        Prova prova = (Prova) intent.getSerializableExtra("prova");

        materia = findViewById(R.id.detalhesProvaMateria);
        data = findViewById(R.id.detalhesProvaData);
        listaTopicos = findViewById(R.id.detalhesProvaTopicos);

        materia.setText(prova.getMateria());
        data.setText(prova.getData());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, prova.getTopicos());

        listaTopicos.setAdapter(adapter);

    }
}
