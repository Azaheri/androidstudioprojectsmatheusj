package br.com.senai.agenda.modelo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import br.com.senai.agenda.ActivityDetalhesProva;
import br.com.senai.agenda.ProvasActivity;
import br.com.senai.agenda.R;
import br.com.senai.agenda.modelo.Prova;

/**
 * Created by mac12 on 05/04/2018.
 */

public class ListaProvasFragments  extends Fragment{
    private ListView listaDeProvas;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.fragments_lista_provas, container, false);


        List<String> topicosPort = Arrays.asList("Sujeito","Objeto direto", "Objeto Indireto");
        Prova provaPortugues = new Prova("Portugues", "03/12/2017", topicosPort);

        List<String> topicosMat = Arrays.asList("Equações de seungdo grau", "Trigonometria");
        Prova provaMatematica = new Prova("Matematica", "27/03/2018", topicosMat);

        final List<Prova> provas = Arrays.asList(provaMatematica, provaPortugues);

        ArrayAdapter<Prova> adapter = new ArrayAdapter<Prova>(getContext(), android.R.layout.simple_list_item_1, provas);

        //listaDeProvas = view.findViewById(R.id.provas_lista);
       // listaDeProvas.setAdapter(adapter);

        listaDeProvas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Prova prova = (Prova) adapterView.getItemAtPosition(i);
                Toast.makeText(getContext(), "Clicou na prova de "+ prova, Toast.LENGTH_LONG).show();
                Intent vaiParaDetalhes = new Intent(getContext(), ActivityDetalhesProva.class);
                vaiParaDetalhes.putExtra("prova", prova);
                startActivity(vaiParaDetalhes);
            }
        });

        return view;
    }
}
