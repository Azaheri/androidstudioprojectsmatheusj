package br.senai.sp.informatica.tablayout.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

import br.senai.sp.informatica.tablayout.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private Button botaoCalendario;
    private View view;

    public DataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_data, container, false);

        botaoCalendario = view.findViewById(R.id.btnData);
        botaoCalendario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragments();
                datePicker.show(getFragmentManager(), "date picker");
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String dataAtual = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
        TextView dataRetornada = view.findViewById(R.id.txtData);
        dataRetornada.setText((CharSequence) dataRetornada);
    }
}
