package br.senai.sp.informatica.tablayout.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.senai.sp.informatica.tablayout.fragments.ContatosFragment;
import br.senai.sp.informatica.tablayout.fragments.ConversasFragment;
import br.senai.sp.informatica.tablayout.fragments.DataFragment;

// FragmentStatePagerAdapter é melhor pois carrega os itens dinamicamente, já o FragmentPagerAdapter é mais pesado pois não faz isso e.e
public class TabAdapter extends FragmentStatePagerAdapter{

    String[] titulosTabs = {"CONVERSAS", "CONTATOS", "DATA"};
    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new ConversasFragment();
                break;
            case 1:
                fragment = new ContatosFragment();
                break;
            case 2:
                fragment = new DataFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return titulosTabs.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titulosTabs[position];
    }
}
