package br.senai.sp.informatica.tablayout;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

class ConfigActivity extends AppCompatActivity{

    private RadioGroup radioGroup;
    private RadioButton radioButtonSelecionado;
    private Button botaoSalvar;
    private static final String ARQUIVO_CONFIG = "arqConfig";
    private ConstraintLayout activityConfiguration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        radioGroup = findViewById(R.id.radioGroupId);
        botaoSalvar = findViewById(R.id.btnSalvar);
        activityConfiguration = findViewById(R.id.activityConfig);

        // Recuperando preferencias
        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_CONFIG, 0);
        if (sharedPreferences.contains("corSelecionada")){
            String corRecuperada = sharedPreferences.getString("corSelecionada", "Padrão");
            setPreferences(corRecuperada);
        }

        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idRadioButtonEscolhido = radioGroup.getCheckedRadioButtonId();

                if (idRadioButtonEscolhido > 0) {
                    SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_CONFIG, 0); // Esse 0 é o MODE_PRIVATE, dão na mesma
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    radioButtonSelecionado = findViewById(idRadioButtonEscolhido);
                    String valorSelecionado = radioButtonSelecionado.getText().toString();
                    editor.putString("corSelecionada", valorSelecionado);
                    editor.commit();
                    setPreferences(valorSelecionado);
                }
            }
        });
    }

    private void setPreferences(String valorSelecionado) {
        switch (valorSelecionado){
            case "Azul":
                activityConfiguration.setBackgroundColor(Color.parseColor("#0000ff"));
                break;
            case "Vermelho":
                activityConfiguration.setBackgroundColor(Color.parseColor("#ff0000"));
                break;
            case "Preto":
                activityConfiguration.setBackgroundColor(Color.parseColor("#000000"));
                break;
            case "Roxo":
                activityConfiguration.setBackgroundColor(Color.parseColor("#800080"));
                break;
            case "Padrão":
                activityConfiguration.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
        }
    }
}
