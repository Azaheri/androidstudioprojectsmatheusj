package br.senai.sp.informatica.agenda.holder;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.senai.sp.informatica.agenda.R;
import br.senai.sp.informatica.agenda.Student;
import br.senai.sp.informatica.agenda.StudentFormActivity;
import br.senai.sp.informatica.agenda.adapter.StudentRecyclerAdapter;

/**
 * Created by 39270271897 on 23/03/2018.
 */

public class StudentViewHolder extends RecyclerView.ViewHolder{

    private TextView nameField;
    private TextView emailField;
    private TextView phoneField;
    private TextView ratingField;
    private TextView addressField;
    private Long studentId;

    public StudentViewHolder(View view, StudentRecyclerAdapter adapter) {
        super(view);

        nameField = view.findViewById(R.id.item_name);
        emailField = view.findViewById(R.id.item_email);
        phoneField = view.findViewById(R.id.item_phone);
        ratingField = view.findViewById(R.id.item_rating);
        addressField = view.findViewById(R.id.item_address);
        view.setOnClickListener(new View.OnClickListener() { // Ao clicar em um elemento da tela, ele armazenará na Activity context
            @Override
            public void onClick(View view) {
                Activity context = (Activity) view.getContext();
                Intent intent = new Intent(context, StudentFormActivity.class);
                intent.putExtra("studentId", studentId);
                context.startActivityForResult(intent, 1);
            }
        });
    }

    public void fill(Student student) {
        studentId = student.getId();
        nameField.setText(student.getName());
        emailField.setText(student.getEmail());
        phoneField.setText(student.getPhoneNumber());
        ratingField.setText(student.getRating().toString());
        if (addressField != null) {
            addressField.setText(student.getAddress());
        }
    }
}
