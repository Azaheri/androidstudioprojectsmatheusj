package br.senai.sp.informatica.agenda;

import android.widget.EditText;
import android.widget.RatingBar;

/**
 * Created by 39270271897 on 16/02/2018.
 * Classe que captura os elementos da tela
 */

public class FormHelper {

    private EditText name;
    private EditText address;
    private EditText email;
    private EditText phoneNumber;
    private EditText facebook;
    private RatingBar ratingBar;
    private Student student;

    public FormHelper(StudentFormActivity form) {
        name = form.findViewById(R.id.editName);
        address = form.findViewById(R.id.editAddress);
        email = form.findViewById(R.id.editEmail);
        phoneNumber = form.findViewById(R.id.editPhoneNumber);
        ratingBar = form.findViewById(R.id.ratingBar2);
        facebook = form.findViewById(R.id.editFace);
        student = new Student();
    }


    public Student getStudent() {
        student.setName(name.getText().toString());
        student.setAddress(address.getText().toString());
        student.setEmail(email.getText().toString());
        student.setPhoneNumber(phoneNumber.getText().toString());
        student.setFacebook(facebook.getText().toString());
        student.setRating(Double.valueOf(ratingBar.getProgress()));
        return student;
    }

    public void fillStudentForm(Student student) {
        name.setText(student.getName());
        address.setText(student.getAddress());
        email.setText(student.getEmail());
        phoneNumber.setText(student.getPhoneNumber());
        facebook.setText(student.getFacebook());
        ratingBar.setProgress(student.getRating().intValue());
        this.student = student;
    }
}
