package br.senai.sp.informatica.agenda;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 39270271897 on 21/02/2018.
 */

public class StudentDAO extends SQLiteOpenHelper{


    public StudentDAO(Context context) {
        super(context, "Agenda", null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE Student(id INTEGER PRIMARY KEY, name TEXT NOT NULL, address TEXT NOT NULL, phoneNumber TEXT NOT NULL, email TEXT NOT NULL, facebook TEXT, rating REAL)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE Student";
        sqLiteDatabase.execSQL(sql);
    }

    public void insert(Student student) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues data = getContentValue(student);
        db.insert("Student", null, data);
    }

    private ContentValues getContentValue(Student student) {
        ContentValues data = new ContentValues();
        data.put("name", student.getName());
        data.put("address", student.getAddress());
        data.put("phoneNumber", student.getPhoneNumber());
        data.put("email", student.getEmail());
        data.put("facebook", student.getFacebook());
        data.put("rating", student.getRating());
        return data;
    }

    public List<Student> searchStudents() {
        String sql = "SELECT * FROM Student";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Student> students = new ArrayList<>();
        while(c.moveToNext()){ //vai usar o cursor para ler os dados linha a linha
            Student student = new Student();
            student.setId(c.getLong(c.getColumnIndex("id")));
            student.setName(c.getString(c.getColumnIndex("name")));
            student.setAddress(c.getString(c.getColumnIndex("address")));
            student.setPhoneNumber(c.getString(c.getColumnIndex("phoneNumber")));
            student.setEmail(c.getString(c.getColumnIndex("email")));
            student.setFacebook(c.getString(c.getColumnIndex("facebook")));
            student.setRating(c.getDouble(c.getColumnIndex("rating")));

            students.add(student);
        }
        return students;
    }

    public void remove(Student student) {
        SQLiteDatabase db = getWritableDatabase();
        String[] param = {student.getId().toString()};
        db.delete("Student", "id = ?", param);
    }

    public void update(Student student) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues data = getContentValue(student);
        String[] param = {student.getId().toString()};
        db.update("Student", data, "id = ?", param);
    }

    public Student Find(Long studentId) {
        String sql = "SELECT * FROM Student WHERE id = ?";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(studentId)});
        c.moveToFirst();
        Student studentFound = new Student();
        studentFound.setId(c.getLong(c.getColumnIndex("id")));
        studentFound.setName(c.getString(c.getColumnIndex("name")));
        studentFound.setAddress(c.getString(c.getColumnIndex("address")));
        studentFound.setPhoneNumber(c.getString(c.getColumnIndex("phoneNumber")));
        studentFound.setEmail(c.getString(c.getColumnIndex("email")));
        studentFound.setFacebook(c.getString(c.getColumnIndex("facebook")));
        studentFound.setRating(c.getDouble(c.getColumnIndex("rating")));
        c.close();
        return studentFound;
    }

    public List<Student> searchByName(String s) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            String sql = "SEELCT * FROM Student WHERE name LIKE '" + s + "%'";
            Cursor c = db.rawQuery(sql, null);
            List<Student> searchedStudent = new ArrayList<>();
            while(c.moveToNext()){ //vai usar o cursor para ler os dados linha a linha
                Student student = new Student();
                student.setId(c.getLong(c.getColumnIndex("id")));
                student.setName(c.getString(c.getColumnIndex("name")));
                student.setAddress(c.getString(c.getColumnIndex("address")));
                student.setPhoneNumber(c.getString(c.getColumnIndex("phoneNumber")));
                student.setEmail(c.getString(c.getColumnIndex("email")));
                student.setFacebook(c.getString(c.getColumnIndex("facebook")));
                student.setRating(c.getDouble(c.getColumnIndex("rating")));

                searchedStudent.add(student);
            }
            c.close();
            return searchedStudent;
        }finally {
            db.close();
        }
    }
}
