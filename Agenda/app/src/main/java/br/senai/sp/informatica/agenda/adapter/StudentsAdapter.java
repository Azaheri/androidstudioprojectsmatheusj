package br.senai.sp.informatica.agenda.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import br.senai.sp.informatica.agenda.MainActivity;
import br.senai.sp.informatica.agenda.R;
import br.senai.sp.informatica.agenda.Student;

/**
 * Created by 39270271897 on 21/03/2018.
 */

public class StudentsAdapter extends BaseAdapter{
    private final Context context;
    private final List<Student> students;

    public StudentsAdapter(Context context, List<Student> students) {
        this.context = context;
        this.students = students;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Object getItem(int i) {
        return students.get(i);
    }

    @Override
    public long getItemId(int i) {
        return students.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Student student = students.get(i);

        LayoutInflater inflater = LayoutInflater.from(context);
        View textView = inflater.inflate(R.layout.main_list, null);

        TextView nameField = textView.findViewById(R.id.item_name);
        nameField.setText(student.getName());
        TextView emailField = textView.findViewById(R.id.item_email);
        emailField.setText(student.getEmail());
        TextView phoneField = textView.findViewById(R.id.item_phone);
        phoneField.setText(student.getPhoneNumber());
        TextView ratingField = textView.findViewById(R.id.item_rating);
        ratingField.setText(student.getRating().toString());
        //Causará nullpointer (logo quando criei a "main_list" em landscape, então quando estiver em portrait, irá crashar sem a condição if
        TextView addressField = textView.findViewById(R.id.item_address);
        if (addressField != null){
            addressField.setText(student.getAddress());
        }
        return textView;
    }
}
