package br.senai.sp.informatica.agenda;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import br.senai.sp.informatica.agenda.adapter.StudentsAdapter;

public class MainActivity extends AppCompatActivity {

    private ListView studentsList;
    private Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        studentsList = findViewById(R.id.lvStudenstList);
        add = findViewById(R.id.btnAdd);

        loadList();

        studentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Student student = (Student) studentsList.getItemAtPosition(i); //Como está retornando um  objeto é necessário fazer cast do mesmo
//              Toast.makeText(MainActivity.this, student.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, StudentFormActivity.class);
                intent.putExtra("student", student);//Objeto Student foi tornado serializable para transmitir o objeto inteiro, ele é transmitido através de binários
                startActivity(intent);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, StudentFormActivity.class);
                startActivity(intent);
            }
        });

        registerForContextMenu(studentsList);
    }//fim onCreate

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem remove = menu.add("Remove");
        remove.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo; //menuInfo passa a posição do item que foi clicado
                Student student = (Student) studentsList.getItemAtPosition(info.position); //um tipo aluno é instanciado e a posição foi passada para o aluno específico
                StudentDAO dao = new StudentDAO(MainActivity.this);
                dao.remove(student);
                dao.close();
                Toast.makeText(MainActivity.this, "Student: "+student.getName()+" was deleted!", Toast.LENGTH_SHORT).show();
                loadList();
                return false;
            }
        });
//        menu.add("Edit");
//        menu.add("Share");

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Student student = (Student) studentsList.getItemAtPosition(info.position);

        MenuItem facebook = menu.add("Facebook Profile");
        Intent intentFace = new Intent(Intent.ACTION_VIEW);
        String startFace = "https://facebook.com/";
        String endFace = student.getFacebook();
        if (!endFace.startsWith("http://www.facebook.com/") && !endFace.startsWith("https://www.facebook.com/")){
            endFace = startFace + endFace;
        }
        intentFace.setData(Uri.parse(endFace));
        facebook.setIntent(intentFace);
//fim facebook

        MenuItem sms = menu.add("Send SMS");
        Intent intentSMS = new Intent(Intent.ACTION_VIEW);
        intentSMS.setData(Uri.parse("sms:" + student.getPhoneNumber()));
        sms.setIntent(intentSMS);
//fim sms
        MenuItem gps = menu.add("Show on Maps");
        Intent intentGPS = new Intent(Intent.ACTION_VIEW);
        intentGPS.setData(Uri.parse("geo:0,0?q=" + student.getAddress()));
        gps.setIntent(intentGPS);
//fim gps
        MenuItem call = menu.add("Call");
        Intent intentCall = new Intent(Intent.ACTION_VIEW);
        intentCall.setData(Uri.parse("tel:" + student.getPhoneNumber()));
        call.setIntent(intentCall);
//fim call


    }

    private void loadList() {
        StudentDAO dao = new StudentDAO(this);
        List<Student> students = dao.searchStudents();
//        ArrayAdapter<Student> adapter = new ArrayAdapter<Student>(this, android.R.layout.simple_list_item_1, students);
        //Será alterada para um adapter criado por nós
        StudentsAdapter adapter = new StudentsAdapter(MainActivity.this, students);
        studentsList.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadList();
    }
    /*@Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
    }*/
}
