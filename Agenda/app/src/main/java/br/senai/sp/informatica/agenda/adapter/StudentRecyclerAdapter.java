package br.senai.sp.informatica.agenda.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.senai.sp.informatica.agenda.MainDrawerActivity;
import br.senai.sp.informatica.agenda.R;
import br.senai.sp.informatica.agenda.Student;
import br.senai.sp.informatica.agenda.holder.StudentViewHolder;

/**
 * Created by 39270271897 on 23/03/2018.
 */

public class StudentRecyclerAdapter extends RecyclerView.Adapter{

    private final Context context;
    private final List<Student> students;

    public StudentRecyclerAdapter(Context context, List<Student> students) {
        this.context = context;
        this.students = students;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_list, parent, false);
        StudentViewHolder holder = new StudentViewHolder(view, this);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StudentViewHolder ourHolder = (StudentViewHolder) holder;
        Student student = students.get(position);
        ourHolder.fill(student);
    }

    @Override
    public int getItemCount() {
        return students.size();
    }
}
