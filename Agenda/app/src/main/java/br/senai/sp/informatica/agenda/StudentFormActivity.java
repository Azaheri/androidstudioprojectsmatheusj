package br.senai.sp.informatica.agenda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

//Classe para insersão de dados que foram pegos do DAO

public class StudentFormActivity extends AppCompatActivity {

    private Button btnAddStudent;
    private FormHelper helper;
    private StudentDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_form);

        btnAddStudent = findViewById(R.id.btnCad);
        helper = new FormHelper(this);

//        Intent intent = getIntent();
//        Student student = (Student) intent.getSerializableExtra("student");
//        if (student != null){
//            helper.fillStudentForm(student);
//        }
        dao = new StudentDAO(this);
        Bundle extras = getIntent().getExtras();
        Long studentId = (extras != null) ? extras.getLong("studentId") : null;
        if (studentId == null){
            Student student = new Student();
        }else{
            Student studentFound = dao.Find(studentId);
            helper.fillStudentForm(studentFound);
        }

        btnAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            StudentDAO dao = new StudentDAO(StudentFormActivity.this);
                Student student = helper.getStudent();
                if(student.getId() != null){
                    dao.update(student);
                }else{
                    dao.insert(student);
                }
                dao.close();
                Toast.makeText(StudentFormActivity.this, "Student: "+student.getName()+ "Saved!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}
