package br.senai.sp.informatica.telas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class segundaTelaActivity extends AppCompatActivity {

    private TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_tela);

        txt = findViewById(R.id.txtSegundaTela);

        Bundle extra = getIntent().getExtras();
        if (extra != null){
            String nameText = extra.getString("name");
            String surnameText = extra.getString("surname");
            txt.setText(nameText+" "+surnameText);
        }
    }
}
