package br.senai.sp.informatica.telas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnSecondScreen;
    private Button btnTerceiraTela;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSecondScreen = findViewById(R.id.btnSegundaTela);
        btnTerceiraTela = findViewById(R.id.btnTerceiraTela);

        btnSecondScreen.setOnClickListener(this);
        btnTerceiraTela.setOnClickListener(this);


//        btnSecondScreen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, segundaTelaActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        btnTerceiraTela.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, terceiraTelaActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        //Uma das maneiras de fazer
//        if (view.getId() == R.id.btnSegundaTela){
//            startActivity(new Intent());
//            //e assim por diante
//        }
        switch (view.getId()){
            case R.id.btnSegundaTela:
                Intent intent = new Intent(MainActivity.this, segundaTelaActivity.class);
                intent.putExtra("name", "Matheus");
                intent.putExtra("surname",  "José Felipe Rosa");
                startActivity(intent);

                break;
            case R.id.btnTerceiraTela:
                startActivity(new Intent(MainActivity.this, terceiraTelaActivity.class));
                break;
        }
    }
}
